output "example1" {
  value = { for s in var.list : s => upper(s) }
}

output "example2" {
  value = [for s in var.list : upper(s) if s != ""]
}
