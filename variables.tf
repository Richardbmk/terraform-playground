# Input Variables

# AWS Region
variable "region" {
  description = "Region in which AWS Resource will be created"
  type        = string
  default     = "us-east-1"
}

variable "worlds" {
  type = list(string)
}

variable "list" {
  type    = list(string)
  default = ["example1", "example2", "example3"]
}
